/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Ing_Rey
 */
public class Alcancia {
    
    
    private int valor=0;
    
    //propiedades almacena unidades de cada denominacion
    private int cantidad_Cincuenta=0;
    private int cantidad_Cien=0;
    private int cantidad_Doscientos=0;
    private int cantidad_Quinientos=0;
    private int cantidad_Mil=0;
    
    private int total_monedas=0;
    
    private int total_en_alcancia=0;

    //propiedades almacenar dinero agregado
    private int cantidad_Cincuenta_agregar=0;
    private int cantidad_Cien_agregar=0;
    private int cantidad_Doscientos_agregar=0;
    private int cantidad_Quinientos_agregar=0;
    private int cantidad_Mil_agregar=0;

    public int getCantidad_Cincuenta_agregar() {
        return cantidad_Cincuenta_agregar;
    }

    public void setCantidad_Cincuenta_agregar(int cantidad_Cincuenta_agregar) {
        this.cantidad_Cincuenta_agregar = cantidad_Cincuenta_agregar;
    }

    public int getCantidad_Cien_agregar() {
        return cantidad_Cien_agregar;
    }

    public void setCantidad_Cien_agregar(int cantidad_Cien_agregar) {
        this.cantidad_Cien_agregar = cantidad_Cien_agregar;
    }

    public int getCantidad_Doscientos_agregar() {
        return cantidad_Doscientos_agregar;
    }

    public void setCantidad_Doscientos_agregar(int cantidad_Doscientos_agregar) {
        this.cantidad_Doscientos_agregar = cantidad_Doscientos_agregar;
    }

    public int getCantidad_Quinientos_agregar() {
        return cantidad_Quinientos_agregar;
    }

    public void setCantidad_Quinientos_agregar(int cantidad_Quinientos_agregar) {
        this.cantidad_Quinientos_agregar = cantidad_Quinientos_agregar;
    }

    public int getCantidad_Mil_agregar() {
        return cantidad_Mil_agregar;
    }

    public void setCantidad_Mil_agregar(int cantidad_Mil_agregar) {
        this.cantidad_Mil_agregar = cantidad_Mil_agregar;
    }
    
    
    
    public Alcancia() {
    }

  
    public int getCantidad_Cincuenta() {
        return cantidad_Cincuenta;
    }

    public void setCantidad_Cincuenta(int cantidad_Cincuenta) {
        
         this.cantidad_Cincuenta = this.cantidad_Cincuenta +cantidad_Cincuenta;
        
    }

    public int getCantidad_Cien() {
        return cantidad_Cien;
    }

    public void setCantidad_Cien(int cantidad_Cien) {
        this.cantidad_Cien = this.cantidad_Cien +cantidad_Cien;
    }

    public int getCantidad_Doscientos() {
        return cantidad_Doscientos;
    }

    public void setCantidad_Doscientos(int cantidad_Doscientos) {
        this.cantidad_Doscientos = this.cantidad_Doscientos+cantidad_Doscientos;
    }

    public int getCantidad_Quinientos() {
        return cantidad_Quinientos;
    }

    public void setCantidad_Quinientos(int cantidad_Quinientos) {
        this.cantidad_Quinientos = this.cantidad_Quinientos+cantidad_Quinientos;
    }

    public int getCantidad_Mil() {
        return cantidad_Mil;
    }

    public void setCantidad_Mil(int cantidad_Mil) {
        this.cantidad_Mil = this.cantidad_Mil+cantidad_Mil;
    }

    public int getTotal_monedas() {
        
        return total_monedas;
    }

    public void setTotal_monedas() {        
        this.total_monedas = this.cantidad_Cincuenta+this.cantidad_Cien+this.cantidad_Doscientos+this.cantidad_Quinientos+this.cantidad_Mil;
    }

    public int getTotal_en_alcancia() {
        return total_en_alcancia;
    }

  
    
    //metodos procesos de la alcancia
    public void agregrar_Dinero(){
         this.total_en_alcancia=total_en_alcancia+ (this.cantidad_Cincuenta_agregar*50)+(this.cantidad_Cien_agregar*100)+(this.cantidad_Doscientos_agregar*200)+(this.cantidad_Quinientos_agregar*500)+(this.cantidad_Mil_agregar*1000);
        
      
                
    }
       public int Saldo(){
         return this.total_en_alcancia;
              
    }
       
       public int getTotal_cincuenta(){
           
          return  this.cantidad_Cincuenta*50;
       
       } 
          public int getTotal_cien(){
           
          return  this.cantidad_Cien *100;
       
       } 
       public int getTotal_doscientos(){
           
          return  this.cantidad_Doscientos *200;
       
       } 
               public int getTotal_quinientos(){
           
          return  this.cantidad_Quinientos *500;
       
       }   
               public int getTotal_mil(){
           
          return  this.cantidad_Mil *1000;
       
       }  
    
}
