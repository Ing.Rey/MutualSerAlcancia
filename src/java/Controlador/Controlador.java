package Controlador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Alcancia;
import javax.servlet.RequestDispatcher;
/**
 *
 * @author Ing_Rey
 */
@WebServlet(urlPatterns = {"/Controlador"})
public class Controlador extends HttpServlet {
    //Instancia Objeto alcancia
Alcancia a= new Alcancia();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>");
             
            out.println("<title>Servlet Controlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    //Usar por Get
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	    response.setContentType("text/html");
        PrintWriter out = response.getWriter();
      
		
		a.setCantidad_Cincuenta(Integer.parseInt(request.getParameter("cincuenta")));
                a.setCantidad_Cien(Integer.parseInt(request.getParameter("cien")));
                a.setCantidad_Doscientos(Integer.parseInt(request.getParameter("doscientos")));
                a.setCantidad_Quinientos(Integer.parseInt(request.getParameter("quinietos")));
                a.setCantidad_Mil(Integer.parseInt(request.getParameter("mil")));
                
                a.setCantidad_Cincuenta_agregar(Integer.parseInt(request.getParameter("cincuenta")));
                a.setCantidad_Cien_agregar(Integer.parseInt(request.getParameter("cien")));
                a.setCantidad_Doscientos_agregar(Integer.parseInt(request.getParameter("doscientos")));
                a.setCantidad_Quinientos_agregar(Integer.parseInt(request.getParameter("quinietos")));
                a.setCantidad_Mil_agregar(Integer.parseInt(request.getParameter("mil")));
	//Redireccionar a la vista
                RequestDispatcher vista;
//capturar mensaje a enviar a la vista
String msn="";
                  //De acuerdo al Boton seleccionado por el usuario se muestra y direcciona a los metodos indicados
                  String op=request.getParameter("bt1");
        switch(op)
        {
        
            case "Agregar Fondos":
                  //Agrega dinero a la alcancia invoca metodo de la clase 
                 //   out.println("Saldo Anterior: "+a.Saldo()+"<br>");
                 a.agregrar_Dinero();
               //  out.println("Nuevo Saldo: "+a.Saldo()+"<br>");
                    msn="Saldo Anterior: "+a.Saldo()+" Nuevo Saldo: "+a.Saldo();
                        request.setAttribute("Mensaje",msn);           
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
              
                break;
              //calcula el Total de unidades de monedas en la alcancia, invoca metodo de la clase 
                 case "Total Monedas":
                     a.setTotal_monedas();   
         
                        msn="Cantidad Monedas en la Alcancia: "+a.getTotal_monedas();
                        request.setAttribute("Mensaje",msn);           
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                break;
                //calcula el saldo invoca metodo de la clase
                  case "Saldo":
     
                //out.println("Total Alcancia: "+a.Saldo()+"<br>");
                      msn="Total Alcancia: "+a.Saldo();
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
              
                break;
                
              case "Total x Denominacion":
                       //total valor por denominacion
                             
                    msn="Total x Denominacion 50: "+a.getTotal_cincuenta()+"<br>"+
                    "Total x Denominacion 100: "+a.getTotal_cien()+"<br>"+
                    "Total x Denominacion 200: "+a.getTotal_doscientos()+"<br>"+
                    "Total x Denominacion 500: "+a.getTotal_quinientos()+"<br>"+
                    "Total x Denominacion 1000: "+a.getTotal_mil();
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                break;    
                case "Total Unds Monedas":
                    //total Unidades de monedas por denominacion
               /*  out.println("Total Und Monedas  50: "+a.getCantidad_Cincuenta()+"<br>");
                 out.println("Total Und Monedas 100: "+a.getCantidad_Cien()+"<br>");
                  out.println("Total Und Monedas 200: "+a.getCantidad_Doscientos()+"<br>");
                   out.println("Total Und Monedas 500: "+a.getCantidad_Quinientos()+"<br>");
                    out.println("Total Und Monedas 1000: "+a.getCantidad_Mil()+"<br>");*/
                    
                     msn="Total Und Monedas  50: "+a.getCantidad_Cincuenta()+"<br>"+
                   "Total Unds Monedas 100: "+a.getCantidad_Cien()+"<br>"+
                    "Total Unds Monedas 200: "+a.getCantidad_Doscientos()+"<br>"+
                    "Total Unds Monedas 500: "+a.getCantidad_Quinientos()+"<br>"+
                   "Total Unds Monedas 1000: "+a.getCantidad_Mil()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 50":
                     //total Unidad por denominacion y monto 
                   
                     msn="Total x Denominacion 50: "+a.getTotal_cincuenta()+ "<br>"+
                         "Total Unds Monedas  50: "+a.getCantidad_Cincuenta()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
           
                    break;
                case "Unds & Monto 100":
                     //total Unidad por denominacion y monto 
                       
                         msn="Total x Denominacion 100: "+a.getTotal_cien()+ "<br>"+
                         "Total Unds Monedas  100: "+a.getCantidad_Cien()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;      
                case "Unds & Monto 200":
                     //total Unidad por denominacion y monto 
                   
                          msn="Total x Denominacion 200: "+a.getTotal_doscientos()+ "<br>"+
                         "Total Unds Monedas  200: "+a.getCantidad_Doscientos()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 500":
                     //total Unidad por denominacion y monto 
                        msn="Total x Denominacion 500: "+a.getTotal_quinientos()+ "<br>"+
                         "Total Unds Monedas 500: "+a.getCantidad_Quinientos()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 1000":
                     //total Unidad por denominacion y monto 
                        msn="Total x Denominacion 1000: "+a.getTotal_mil()+ "<br>"+
                        "Total Unds Monedas 1000: "+a.getCantidad_Mil()+"<br>";
            
                         request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                    
                default:
                            msn="Buenas...";
            
                         request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                        break;
                            
                    
        }
 
                 
	//out.println("<a href='index.html'>Regresar</a>");
		
		 out.close();
	}

     //Usar por POST
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	 response.setContentType("text/html");
        PrintWriter out = response.getWriter();
      
		//Valores inciales a las propiedades de la clase
		a.setCantidad_Cincuenta(Integer.parseInt(request.getParameter("cincuenta")));
                a.setCantidad_Cien(Integer.parseInt(request.getParameter("cien")));
                a.setCantidad_Doscientos(Integer.parseInt(request.getParameter("doscientos")));
                a.setCantidad_Quinientos(Integer.parseInt(request.getParameter("quinietos")));
                a.setCantidad_Mil(Integer.parseInt(request.getParameter("mil")));
                
                a.setCantidad_Cincuenta_agregar(Integer.parseInt(request.getParameter("cincuenta")));
                a.setCantidad_Cien_agregar(Integer.parseInt(request.getParameter("cien")));
                a.setCantidad_Doscientos_agregar(Integer.parseInt(request.getParameter("doscientos")));
                a.setCantidad_Quinientos_agregar(Integer.parseInt(request.getParameter("quinietos")));
                a.setCantidad_Mil_agregar(Integer.parseInt(request.getParameter("mil")));
		//Redireccionar a la vista
                RequestDispatcher vista;
//capturar mensaje a enviar a la vista
String msn="";
                  //De acuerdo al Boton seleccionado por el usuario se muestra y direcciona a los metodos indicados
                  String op=request.getParameter("bt1");
        switch(op)
        {
        
            case "Agregar Fondos":
                  //Agrega dinero a la alcancia invoca metodo de la clase 
                 //   out.println("Saldo Anterior: "+a.Saldo()+"<br>");
                 a.agregrar_Dinero();
               //  out.println("Nuevo Saldo: "+a.Saldo()+"<br>");
                    msn="Saldo Anterior: "+a.Saldo()+" Nuevo Saldo: "+a.Saldo();
                        request.setAttribute("Mensaje",msn);           
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
              
                break;
              //calcula el Total de unidades de monedas en la alcancia, invoca metodo de la clase 
                 case "Total Monedas":
                     a.setTotal_monedas();   
         
                        msn="Cantidad Monedas en la Alcancia: "+a.getTotal_monedas();
                        request.setAttribute("Mensaje",msn);           
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                break;
                //calcula el saldo invoca metodo de la clase
                  case "Saldo":
     
                //out.println("Total Alcancia: "+a.Saldo()+"<br>");
                      msn="Total Alcancia: "+a.Saldo();
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
              
                break;
                
              case "Total x Denominacion":
                       //total valor por denominacion
                             
                    msn="Total x Denominacion 50: "+a.getTotal_cincuenta()+"<br>"+
                    "Total x Denominacion 100: "+a.getTotal_cien()+"<br>"+
                    "Total x Denominacion 200: "+a.getTotal_doscientos()+"<br>"+
                    "Total x Denominacion 500: "+a.getTotal_quinientos()+"<br>"+
                    "Total x Denominacion 1000: "+a.getTotal_mil();
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                break;    
                case "Total Unds Monedas":
                    //total Unidades de monedas por denominacion
               /*  out.println("Total Und Monedas  50: "+a.getCantidad_Cincuenta()+"<br>");
                 out.println("Total Und Monedas 100: "+a.getCantidad_Cien()+"<br>");
                  out.println("Total Und Monedas 200: "+a.getCantidad_Doscientos()+"<br>");
                   out.println("Total Und Monedas 500: "+a.getCantidad_Quinientos()+"<br>");
                    out.println("Total Und Monedas 1000: "+a.getCantidad_Mil()+"<br>");*/
                    
                     msn="Total Unds Monedas  50: "+a.getCantidad_Cincuenta()+"<br>"+
                   "Total Unds Monedas 100: "+a.getCantidad_Cien()+"<br>"+
                    "Total Unds Monedas 200: "+a.getCantidad_Doscientos()+"<br>"+
                    "Total Unds Monedas 500: "+a.getCantidad_Quinientos()+"<br>"+
                   "Total Unds Monedas 1000: "+a.getCantidad_Mil()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 50":
                     //total Unidad por denominacion y monto 
                   
                     msn="Total x Denominacion 50: "+a.getTotal_cincuenta()+ "<br>"+
                         "Total Unds Monedas  50: "+a.getCantidad_Cincuenta()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
           
                    break;
                case "Unds & Monto 100":
                     //total Unidad por denominacion y monto 
                       
                         msn="Total x Denominacion 100: "+a.getTotal_cien()+ "<br>"+
                         "Total Unds Monedas  100: "+a.getCantidad_Cien()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;      
                case "Unds & Monto 200":
                     //total Unidad por denominacion y monto 
                   
                          msn="Total x Denominacion 200: "+a.getTotal_doscientos()+ "<br>"+
                         "Total Unds Monedas  200: "+a.getCantidad_Doscientos()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 500":
                     //total Unidad por denominacion y monto 
                        msn="Total x Denominacion 500: "+a.getTotal_quinientos()+ "<br>"+
                         "Total Unds Monedas 500: "+a.getCantidad_Quinientos()+"<br>";
                        
                        request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                case "Unds & Monto 1000":
                     //total Unidad por denominacion y monto 
                        msn="Total x Denominacion 1000: "+a.getTotal_mil()+ "<br>"+
                        "Total Unds Monedas 1000: "+a.getCantidad_Mil()+"<br>";
            
                         request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                    break;
                default:
                            msn="Buenas...";
            
                         request.setAttribute("Mensaje",msn);            
                        vista=request.getRequestDispatcher("index.jsp");
                        vista.forward(request, response);
                        break;
                    
        }
 
                 
	//out.println("<a href='index.html'>Regresar</a>");
		
		 out.close();
	}
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
