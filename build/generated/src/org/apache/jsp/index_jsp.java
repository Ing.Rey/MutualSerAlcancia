package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>TODO supply a title</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("         <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\n");
      out.write("         <link rel=\"stylesheet\" href=\"Style.css\" type=\"text/css\">\n");
      out.write("           <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n");
      out.write("            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>\n");
      out.write("            <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"jumbotron\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                <h1 class=\"bg-primary text-white\">Mutual Alcancia</h1>\n");
      out.write("                \n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        <form name=\"form\" method=\"post\" action=\"Controlador\">\n");
      out.write("            <div class=\"container\" id=\"div_izquierda\">\n");
      out.write("            <table class=\"table table-dark table-hover\" > \n");
      out.write("                <tr>\n");
      out.write("                    <td> <h1 id=\"texto\">50</h1></td>\n");
      out.write("                    <td><input id=\"cajas_texto\" type=\"number\" name=\"cincuenta\" min=\"0\" max=\"100\" step=\"1\" value=\"0\" class=\"form-control\" ></td>  \n");
      out.write("             </tr>\n");
      out.write("             <tr>\n");
      out.write("                 <td><h1 id=\"texto\">100</h1></td>\n");
      out.write("                     <td><input id=\"cajas_texto\" type=\"number\" name=\"cien\" min=\"0\" max=\"100\" step=\"1\" value=\"0\" class=\"form-control\"></td> \n");
      out.write("             <tr>\n");
      out.write("                    <td><h1 id=\"texto\">200</h1></td>\n");
      out.write("                     <td><input id=\"cajas_texto\" type=\"number\" name=\"doscientos\" min=\"0\" max=\"100\" step=\"1\" value=\"0\" class=\"form-control\"/></td> \n");
      out.write("                    </tr>\n");
      out.write("             <tr>\n");
      out.write("                    <td><h1 id=\"texto\">500</h1></td>\n");
      out.write("                     <td><input id=\"cajas_texto\" type=\"number\" name=\"quinietos\" min=\"0\" max=\"100\" step=\"1\" value=\"0\" class=\"form-control\"/></td> \n");
      out.write("                    </tr>\n");
      out.write("             <tr>\n");
      out.write("                    <td><h1 id=\"texto\">1000</h1></td>\n");
      out.write("                     <td><input id=\"cajas_texto\" type=\"number\" name=\"mil\" min=\"0\" max=\"100\" step=\"1\" value=\"0\" class=\"form-control\"/></td> \n");
      out.write("                    </tr>\n");
      out.write("             \n");
      out.write("            </table>   \n");
      out.write("            </div>\n");
      out.write("           \n");
      out.write("            \n");
      out.write("            <div class=\"container\" id=\"div_derecha\">\n");
      out.write("            <table class=\"table table-hover\">\n");
      out.write("                  <tr> \n");
      out.write("                      <td id=\"centrar\">\n");
      out.write("                           \n");
      out.write("                        <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Agregar Fondos\" class=\"btn btn-primary\">\n");
      out.write("                 \n");
      out.write("                        <input id=\"botones\" type=\"submit\"  name=\"bt1\" value=\"Total Monedas\"  class=\"btn btn-light\"/>\n");
      out.write("                    \n");
      out.write("                         <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Saldo\" class=\"btn btn-primary\">                         \n");
      out.write("                    \n");
      out.write("                         <input id=\"botones\"  type=\"submit\" name=\"bt1\" value=\"Total Unds Monedas\"  class=\"btn btn-light\"/>  \n");
      out.write("                  \n");
      out.write("                        <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Total x Denominacion\" class=\"btn btn-primary\"/>\n");
      out.write("                 \n");
      out.write("                          <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Unds & Monto 50\"  class=\"btn btn-light\"/>\n");
      out.write("                  \n");
      out.write("                         <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Unds & Monto 100\" class=\"btn btn-primary\"/>\n");
      out.write("                  \n");
      out.write("                        <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Unds & Monto 200\"  class=\"btn btn-light\"/>\n");
      out.write("                   \n");
      out.write("                        <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Unds & Monto 500\" class=\"btn btn-primary\"/>\n");
      out.write("                    \n");
      out.write("                        <input id=\"botones\" type=\"submit\" name=\"bt1\" value=\"Unds & Monto 1000\"  class=\"btn btn-light\"/>\n");
      out.write("                          \n");
      out.write("                    </td>\n");
      out.write("                </tr>\n");
      out.write("                <tr> \n");
      out.write("                    <td id=\"centrar\">\n");
      out.write("                        \n");
      out.write("          <div class=\"container\"> \n");
      out.write("                  <div class=\"alert alert-info alert-dismissible fade show\">\n");
      out.write("                 <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n");
      out.write("                 Información\n");
      out.write("                ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.Mensaje}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("              </div>\n");
      out.write("                        \n");
      out.write("                    </td>\n");
      out.write("                </tr>\n");
      out.write("                \n");
      out.write("                <tr> \n");
      out.write("                    <td id=\"centrar\">\n");
      out.write("                        <img src=\"../imgenes/logomutual.png\">\n");
      out.write("                        <img src=\"logoalcancia.png\">\n");
      out.write("                    </td>\n");
      out.write("                </tr>\n");
      out.write("            </table>    \n");
      out.write("            </div>  \n");
      out.write("        </form>       \n");
      out.write("              \n");
      out.write("              </div>\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
