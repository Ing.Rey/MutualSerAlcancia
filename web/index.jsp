<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Mutual Ser Alcancia</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
         <link rel="stylesheet" href="css/Style.css" type="text/css">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">           
                <h3 class="bg-primary text-white">Mutual Alcancia</h3>
                   <p> &copy Reyner Fernando Mu�oz De Paz - 2020</p> 
               </div>  
            </div>

        <form name="form" method="post" action="Controlador">
            <div class="container" id="div_izquierda">
            <table class="table table-dark table-hover" > 
                <tr>
                    <td><h1 id="texto"><img id="imagen_dimenciones" src="Imagenes/moneda.png"/> 50</h1></td>
                    <td><input id="cajas_texto" type="number" name="cincuenta" min="0" max="100" step="1" value="0" class="form-control" ></td>  
             </tr>
             <tr>
                 <td><h1 id="texto"><img id="imagen_dimenciones" src="Imagenes/moneda.png"/> 100</h1></td>
                     <td><input id="cajas_texto" type="number" name="cien" min="0" max="100" step="1" value="0" class="form-control"></td> 
             <tr>
                    <td><h1 id="texto"><img id="imagen_dimenciones" src="Imagenes/moneda.png"/> 200</h1></td>
                     <td><input id="cajas_texto" type="number" name="doscientos" min="0" max="100" step="1" value="0" class="form-control"/></td> 
                    </tr>
             <tr>
                    <td><h1 id="texto"><img id="imagen_dimenciones" src="Imagenes/moneda.png"/> 500</h1></td>
                     <td><input id="cajas_texto" type="number" name="quinietos" min="0" max="100" step="1" value="0" class="form-control"/></td> 
                    </tr>
             <tr>
                    <td><h1 id="texto"><img id="imagen_dimenciones" src="Imagenes/moneda.png"/> 1.000</h1></td>
                     <td><input id="cajas_texto" type="number" name="mil" min="0" max="100" step="1" value="0" class="form-control"/></td> 
                    </tr>
             
            </table>   
            </div>        
            
            <div class="container" id="div_derecha">
            <table class="table table-hover">
                  <tr> 
                      <td id="centrar">
                           
                        <input id="botones" type="submit" name="bt1" value="Agregar Fondos" class="btn btn-primary">
                 
                        <input id="botones" type="submit"  name="bt1" value="Total Monedas"  class="btn btn-light"/>
                    
                         <input id="botones" type="submit" name="bt1" value="Saldo" class="btn btn-primary">                         
                    
                         <input id="botones"  type="submit" name="bt1" value="Total Unds Monedas"  class="btn btn-light"/>  
                  
                        <input id="botones" type="submit" name="bt1" value="Total x Denominacion" class="btn btn-primary"/>
                 
                          <input id="botones" type="submit" name="bt1" value="Unds & Monto 50"  class="btn btn-light"/>
                  
                         <input id="botones" type="submit" name="bt1" value="Unds & Monto 100" class="btn btn-primary"/>
                  
                        <input id="botones" type="submit" name="bt1" value="Unds & Monto 200"  class="btn btn-light"/>
                   
                        <input id="botones" type="submit" name="bt1" value="Unds & Monto 500" class="btn btn-primary"/>
                    
                        <input id="botones" type="submit" name="bt1" value="Unds & Monto 1000"  class="btn btn-light"/>
                          
                    </td>
                </tr>
                <tr> 
                    <td id="centrar">
                        
          <div class="container"> 
                  <div class="alert alert-info alert-dismissible fade show">
                 <button type="button" class="close" data-dismiss="alert">&times;</button>
                 Informaci�n
                ${requestScope.Mensaje}
              </div>
                        
                    </td>
                </tr>
                
                <tr> 
                    <td id="centrar">
                        <img src="Imagenes/logomutual.png">
                        <img src="Imagenes/logoalcancia.png">
                    </td>
                </tr>
            </table>    
            </div>  
        </form>       
              
              </div>
    </body>

</html>
